import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import { IoPersonOutline, IoLogOutOutline } from "react-icons/io5";
import PersonIcon from "@mui/icons-material/Person";
import { Toast } from "primereact/toast";
import style from "./MainHeader.module.css";
import { useNavigate } from "react-router-dom";

export const MainHeader = ({
  title = "",
  actionButton = false,
  toggleSidebar,
  isSidebarOpen,
}) => {
  const [menuActive, setMenuActive] = useState(false);
  const navigate = useNavigate();
  const handleLogout = () => {
    navigate("/")
  };

  return (
    <>
      <header className={style.mainHeader__container}>
        <div style={{ display: "flex", alignItems: "center", gap: "20px" }}>
          <i
            style={{ cursor: "pointer", fontSize: "1.1rem" }}
            className={`pi pi-bars`}
            onClick={toggleSidebar}
          />
          <div style={{ display: "flex", gap: "10px", alignItems: "center" }}>
            <p className={style.mainHeader__title}>BACKOFFICE</p>
          </div>
        </div>

        <div className={style.mainHeader__navbar__container}>
          {/* Aquí puedes agregar elementos del menú si es necesario */}
          <div style={{ position: "relative" }}>
            <div
              className={style.mainHeader__profile}
              onClick={() => setMenuActive((prev) => !prev)}
            >
              <PersonIcon />
            </div>
            {menuActive && (
              <div className={style.profileMenu}>
                <ul className={style.profileMenu__list}>
                  <li className={style.profileMenu__item} onClick={handleLogout}>
                    <IoLogOutOutline size={20} /> Cerrar Sesión
                  </li>
                </ul>
              </div>
            )}
          </div>
        </div>
      </header>
    </>
  );
};
