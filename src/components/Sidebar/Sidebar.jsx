import { Drawer, List } from "@mui/material";
import SidebarItem from "./SidebarItem";
import SidebarItemCollapse from "./SidebarItemCollapse";
import style from "./Sidebar.module.css";
import { useSelector } from "react-redux";

const Sidebar = ({ appRoutes, isSidebarOpen, toggleSidebar, specialRoute, RouteUser }) => {
  const userRole = useSelector((state) => state.auth.login?.user?.role);
  const isAdminRoot = userRole === "root";

  const sidebarRoutes = isAdminRoot ? specialRoute : RouteUser.concat(appRoutes);

  return (
    <>
      {isSidebarOpen && appRoutes && (
        <div className={style.container__drawer}>
          <List disablePadding>
            {sidebarRoutes.map((route, index) =>
              route.sidebarProps ? (
                route.child ? (
                  <SidebarItemCollapse item={route} key={index} />
                ) : (
                  <SidebarItem item={route} key={index} />
                )
              ) : null
            )}
          </List>
        </div>
      )}
    </>
  );
};

export default Sidebar;
