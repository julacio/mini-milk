import React from "react";
import style from "./TextBoxField.module.css";

import { InputLabel, TextField } from "@mui/material";

export const TextBoxField = ({ textLabel, value, name, type = "text", onChange, placeholder, disabled }) => {
	return (
		<div className={style.column__item}>
			<InputLabel>{textLabel}</InputLabel>

			<TextField
			disabled={disabled}
				value={value}
				name={name}
				type={type}
				onChange={onChange}
				size="small"
				fullWidth
				autoComplete="off"
				placeholder={placeholder}
			/>
		</div>
	);
};
