import { configureStore } from '@reduxjs/toolkit';
import { authSlice } from './slices/auth/authSlice';
import sidebarReducer from "./slices/sidebar/IsSidebarOpen";
import dataReducer, { dataServicesSlice } from './slices/servicesData/ServicesSlice';
import dataMallaReducer, { dataMallaSlice } from './slices/mallaData/MallaSlice'; //

export const store = configureStore({
  reducer: {
    auth: authSlice.reducer,
    sidebar: sidebarReducer,
    dataServices: dataServicesSlice.reducer,
    dataMalla: dataMallaSlice.reducer, 
  },
});