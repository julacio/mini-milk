import { MainContentStructure } from "@/components/MainContentStructure/MainContentStructure";
import { SectionStructure } from "@/components/SectionStructure/SectionStructure";
import { Button } from 'primereact/button';
import axios from "axios";
import { url } from "@/connections/mainApi";
import useModal from "@/hooks/useModal";
import { DataTable } from "@/components/DataTable/DataTable";
import CrearGasto from "./components/CrearGasto";


function Gastos() {
  const { showModal, hideModal, Modal } = useModal()


  const columns = [
    { nombre: "ID", campo: "id" },
    { nombre: "Ingresos Semanales", campo: "ingreso" },
    { nombre: "Gastos Semanales", campo: "gastos" },
    { nombre: "Rentabilidad", campo: "rentabilidad" },
    { nombre: "Fecha", campo: "fecha" },

  ];

  const data = [
    {id: 0, ingreso: "2.200.000", gastos: "1.200.000", rentabilidad: "1.000.000", fecha: "19/10/23" },
    {id: 1, ingreso: "2.200.000", gastos: "1.200.000", rentabilidad: "1.000.000", fecha: "26/10/23" },
    {id: 2, ingreso: "2.200.000", gastos: "1.200.000", rentabilidad: "1.000.000", fecha: "02/11/23" },
    {id: 3, ingreso: "2.200.000", gastos: "1.200.000", rentabilidad: "1.000.000", fecha: "09/11/23" },
  ]

  const handleDelete = async( id ) => {
    const URL = `${url}/etiqueta/${id}`
    try {
      const response = await axios.delete(URL)
      setEtiquetasData(response.data)
    } catch (error) {
      console.log(error)
      
    }
  }
  
  const buttonDelete = (rowData) => {
    return (
      <Button
        className="p-button-info p-button-rounded"
        style={{ width: "40px", height: "40px", backgroundColor: "red" }}
        type="button"
        icon="pi pi-trash"
        onClick={()=> handleDelete(rowData.id_etiqueta)}
      />
    );
  };



  return (  
    <>
      <MainContentStructure>
        <SectionStructure>
        <h2 className="title__sections">Gestión de Gastos / Ingresos</h2>
        <hr />

        <DataTable
        showEditButton={true}
        buttonEye={true}
        columns={columns}
        data={data}
        textAddButton={"Añadir Rentabilidad"}
        onAddModal={showModal}
        showDeleteButton={true}
        buttonDecline={buttonDelete}
        />

        </SectionStructure>
      </MainContentStructure>
      <Modal title={"Añadir Rentabilidad"} width={600}>
        <CrearGasto />
      </Modal>
    </>
  );
}

export default Gastos;