import { MainContentStructure } from "@/components/MainContentStructure/MainContentStructure";
import { TextBoxField } from "@/components/TextBoxField/TextBoxField";
import { useState } from "react";
import style from './CrearGasto.module.css'

const initialState = {
  animal: "",
  produccion_semanal: "",
  estado: true,
  precio: "",
  raza: "",
  vida_util: ""
}

function CrearGasto() {
  const [animal, setAnimal] = useState(initialState);

  const handleChange = (e) => {
    const { name, value } = e.target

    setAnimal({
      ...animal, 
      [name]: value
    })
  }

  return (  
    <MainContentStructure>
      <div className={style.container__1}>
        <TextBoxField
          name="animal"
          value={animal.animal}
          onChange={handleChange}
          textLabel={"Ingreso Semanal"}
          placeholder={"Ingresa tu Ingreso"}
          />

        <TextBoxField
          name="produccion_semanal"
          value={animal.produccion_semanal}
          onChange={handleChange}
          textLabel={"Gasto Semanal"}
          placeholder={"Ingresa tu Gasto Semanal"}
          />

      </div>
      <div className={style.container__1}>

        </div>
          <TextBoxField
            name="precio"
            value={animal.precio}
            onChange={handleChange}
            textLabel={"Detalles de Gasto"}
            placeholder={"¿En qué Gastaste está semana?"}
          />

        <div className={style.container__2}>
          <button> Añadir </button>
        </div>

    </MainContentStructure>
  );
}

export default CrearGasto;