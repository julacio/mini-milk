import { DataTable } from "@/components/DataTable/DataTable";
import { MainContentStructure } from "@/components/MainContentStructure/MainContentStructure";
import { Button } from 'primereact/button';
import axios from "axios";
import { url } from "@/connections/mainApi";
import useModal from "@/hooks/useModal";
import CrearAnimal from "./components/CrearAnimal";

function Animales() {
  const { showModal, hideModal, Modal } = useModal()


  const columns = [
    { nombre: "ID", campo: "id" },
    { nombre: "Animal", campo: "nombre" },
    { 
      nombre: "Estado", 
      campo: "estado_etiqueta",
      body: (rowData) => rowData.estado_etiqueta ? "Activo" : "Inactivo"
    },
    { nombre: "Producción Semanal", campo: "produccion" },
    { nombre: "Vida Útil", campo: "vida_util" },

  ];

  const data = [
    {id: 0, nombre: "Vaca 1", estado: true, produccion: 150, vida_util: 2 },
    {id: 1, nombre: "Vaca 2", estado: true, produccion: 150, vida_util: 2 },
    {id: 2, nombre: "Vaca 3", estado: true, produccion: 150, vida_util: 2 },
    {id: 3, nombre: "Vaca 4", estado: true, produccion: 150, vida_util: 2 },
    {id: 4, nombre: "Vaca 5", estado: true, produccion: 150, vida_util: 2 },

  ]

  const handleDelete = async( id ) => {
    const URL = `${url}/etiqueta/${id}`
    try {
      const response = await axios.delete(URL)
      setEtiquetasData(response.data)
    } catch (error) {
      console.log(error)
      
    }
  }
  
  const buttonDelete = (rowData) => {
    return (
      <Button
        className="p-button-info p-button-rounded"
        style={{ width: "40px", height: "40px", backgroundColor: "red" }}
        type="button"
        icon="pi pi-trash"
        onClick={()=> handleDelete(rowData.id_etiqueta)}
      />
    );
  };


  return (  
    <>
      <MainContentStructure>
        <h2 className="title__sections">Gestión de Animales</h2>
        <hr />

        <DataTable
            showEditButton={true}
            buttonEye={true}
            columns={columns}
            data={data}
            textAddButton={"Añadir Animal"}
            onAddModal={showModal}
            showDeleteButton={true}
            buttonDecline={buttonDelete}
            />

      </MainContentStructure>

      <Modal title={"Añadir Animal"} width={600}>
        <CrearAnimal hideModal={hideModal}/>
      </Modal>
    </>
  );
}

export default Animales;


