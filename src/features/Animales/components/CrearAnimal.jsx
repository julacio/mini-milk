import { MainContentStructure } from "@/components/MainContentStructure/MainContentStructure";
import { TextBoxField } from "@/components/TextBoxField/TextBoxField";
import { useState } from "react";
import style from '../Animales.module.css'

const initialState = {
  animal: "",
  produccion_semanal: "",
  estado: true,
  precio: "",
  raza: "",
  vida_util: ""
}

function CrearAnimal( { hideModal } ) {
  const [animal, setAnimal] = useState(initialState);

  const handleChange = (e) => {
    const { name, value } = e.target

    setAnimal({
      ...animal, 
      [name]: value
    })
  }

  return (  
    <MainContentStructure>
      <div className={style.container__1}>
        <TextBoxField
          name="animal"
          value={animal.animal}
          onChange={handleChange}
          textLabel={"Animal"}
          placeholder={"Ingresa un nombre o tipo"}
          />

        <TextBoxField
          name="produccion_semanal"
          value={animal.produccion_semanal}
          onChange={handleChange}
          textLabel={"Producción Semanal"}
          placeholder={"Ingresa tu producción semanal en Litros"}
          />

      </div>
      <div className={style.container__1}>

          <TextBoxField
            name="precio"
            value={animal.precio}
            onChange={handleChange}
            textLabel={"Precio del Animal (OPCIONAL)"}
            placeholder={"Ingresa el Precio en Pesos"}
          />

          <TextBoxField
            name="raza"
            value={animal.raza}
            onChange={handleChange}
            textLabel={"Raza del Animal (OPCIONAL)"}
            placeholder={"Ingresa la Raza"}
          />

        </div>
          <TextBoxField
            name="vida_util"
            value={animal.vida_util}
            onChange={handleChange}
            textLabel={"Vida Productiva (OPCIONAL)"}
            placeholder={"Ingresa la Vida Productiva"}
          />

        <div className={style.container__2}>
          <button> Añadir </button>
        </div>

    </MainContentStructure>
  );
}

export default CrearAnimal;