import { MainContentStructure } from "@/components/MainContentStructure/MainContentStructure";
import style from "./Dashboard.module.css";
import BarChart from "@/components/BarChart/BarChart";
import LineChart from "@/components/LineChart/LineChart";
import TrendingUpIcon from "@mui/icons-material/TrendingUp";
import LocalGasStationIcon from "@mui/icons-material/LocalGasStation";
import CheckIcon from "@mui/icons-material/Check";
import HourglassEmptyIcon from "@mui/icons-material/HourglassEmpty";
import FlagIcon from "@mui/icons-material/Flag";
import AttachMoneyIcon from "@mui/icons-material/AttachMoney";
import PersonIcon from "@mui/icons-material/Person";
import { useSelector } from "react-redux";
import { useQuery } from "react-query";
import axios from "axios";
import { useDispatch } from "react-redux";
import { setdataServices } from "@/store/slices/servicesData/ServicesSlice";
import { useEffect, useState } from "react";

export const Dashboard = () => {
  return (
    <>
      <MainContentStructure>
        <h2 className="title__sections">Dashboard</h2>
        <hr />
        <div
          style={{ display: "flex", flexDirection: "column", gap: "1.5rem" }}
        >
          <div className={style.section__dashboard__container1}>
            <div
              className={`${style.section_structure} ${style.section1} section_structure`}
            >
              <div className={style.dashboard_resume_data}>
                <h3>
                  Producción Esperada Semanal
                  <CheckIcon
                    sx={{
                      color: "black",
                      backgroundColor: "white",
                      borderRadius: "50%",
                      fontSize: "19px",
                      margin: "0 5px",
                      padding: "2px",
                      fontWeigth: "bold",
                    }}
                  />
                </h3>

                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    margin: "0.5rem 0",
                  }}
                >
                  <LocalGasStationIcon
                    sx={{ color: "white", fontSize: "2.5rem" }}
                  />
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "start",
                      margin: "0 0.2rem",
                    }}
                  >
                    <p style={{ fontWeight: "500", fontFamily: "Roboto" }}>
                      COP/. 2.500.000
                    </p>
                    <p style={{ fontWeight: "500", fontFamily: "Roboto" }}>
                      Litros 560 
                    </p>
                  </div>
                </div>
              </div>
            </div>

            <div
              className={`${style.section_structure} ${style.section2} section_structure`}
            >
              <div className={style.dashboard_resume_data}>
                <h3>
                  Produccion Actual (3 días de 7){" "}
                  <HourglassEmptyIcon
                    sx={{
                      color: "#232c3d",
                      backgroundColor: "white",
                      borderRadius: "50%",
                      fontSize: "19px",
                      margin: "0 5px",
                      padding: "2px",
                    }}
                  />
                </h3>

                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    margin: "0.5rem 0",
                  }}
                >
                  <LocalGasStationIcon
                    sx={{ color: "white", fontSize: "2.5rem" }}
                  />
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "start",
                      margin: "0 0.2rem",
                    }}
                  >
                    <p style={{ fontWeight: "500", fontFamily: "Roboto" }}>
                      COP/. 1.250.000
                    </p>
                    <p style={{ fontWeight: "500", fontFamily: "Roboto" }}>
                      Litros 280
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className={style.section__dashboard__container}>
            <div
              className={`${style.section_structure} ${style.section3} section_structure`}
            >
              <div className={style.dashboard_resume_data}>
                <h3>
                  Meta Diaria Litros{" "}
                  <FlagIcon
                    sx={{
                      color: "#232c3d",
                      backgroundColor: "white",
                      borderRadius: "50%",
                      fontSize: "19px",
                      margin: "0 2px",
                      padding: "2px",
                    }}
                  />
                </h3>
                <span className={style.data_span}>
                  {" "}
                  <LocalGasStationIcon
                    sx={{ color: "white", fontSize: "1.7rem" }}
                  />{" "}
                  600
                </span>
              </div>
            </div>

            <div
              className={`${style.section_structure} ${style.section4} section_structure`}
            >
              <div className={style.dashboard_resume_data}>
                <h3> Costes Mensuales</h3>
                <span className={style.data_span}>
                  {" "}
                  <TrendingUpIcon
                    sx={{ color: "#53e72a", fontSize: "1rem" }}
                  />{" "}
                  
                  <p
                    style={{
                      fontWeight: "500",
                      fontFamily: "Roboto",
                      fontSize: "1rem",
                    }}
                  >
                    COP. 1.250.000
                  </p>
                </span>
              </div>
            </div>

            <div
              className={`${style.section_structure} ${style.section5} section_structure`}
            >
              <div className={style.dashboard_resume_data}>
                <h3>
                  Ganancia Mensual{" "}
                  <AttachMoneyIcon
                    sx={{
                      color: "#232c3d",
                      backgroundColor: "white",
                      borderRadius: "50%",
                      fontSize: "21px",
                      margin: "0 2px",
                      padding: "2px",
                      fontWeigth: "bold",
                    }}
                  />
                </h3>
                <span className={style.data_span}>COP. 2.500.000 </span>
              </div>
            </div>
          </div>
        </div>
        <div className={style.dashboard_charts}>
          <div className={style.item__dashboard__charts}>
            <div className={style.chart_1}>
              {" "}
              <LineChart
                data={[]}
                totalExitosas={[]}
                totalPendientes={[]}
              />
            </div>
          </div>
        </div>
      </MainContentStructure>
    </>
  );
};
