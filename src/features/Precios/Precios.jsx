import { MainContentStructure } from "@/components/MainContentStructure/MainContentStructure";
import { SectionStructure } from "@/components/SectionStructure/SectionStructure";
import { useState } from "react";
import styled from './Precio.module.css'
import useModal from "@/hooks/useModal";
import { TextBoxField } from "@/components/TextBoxField/TextBoxField";

function Precios() {
  const { showModal, hideModal, Modal } = useModal();
  const [precio, setPrecio] = useState(4600);
  const [change, setChange] = useState(4600); // Estado local para el input del modal

  const handleModal = () => {
    setChange(precio); // Restablecer el estado local al valor actual de 'precio'
    showModal(true);
  }

  const handleChange = (e) => {
    const { value } = e.target;
    setChange(value); // Actualizar el estado local cuando se cambia el input
  }

  const handleSubmit = () => {
    setPrecio(change);
    hideModal(); // Cerrar el modal después de cambiar el precio
  }

  return (  
    <>
      <MainContentStructure>
        <SectionStructure>
          <h2 className="title__sections">Gestión de Precios</h2>
          <hr />

          <div className={styled.container__precio}>
            <div>
              {`El precio de la Leche Actual es: ${precio}`}
            </div>
          </div>

          <div className={styled.container__button}>
            <button onClick={handleModal}> Cambiar </button>
          </div>
        </SectionStructure>
      </MainContentStructure>

      <Modal title={"Editar Precio"} width={600}>
        <TextBoxField
          name={"precio"}
          onChange={handleChange}
          value={change} // Mostrar el valor de 'change' en el input
        />

        <div className={styled.container__button}>
          <button onClick={handleSubmit}> Cambiar </button>
        </div>
      </Modal>
    </>
  );
}

export default Precios;
