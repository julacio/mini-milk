import React, { useState } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import user0Icon from "../assets/User0.svg";
import hideIcon from "../assets/Hide.svg";
import loginIcon from "../assets/Login.svg";
import style from "./ContenedorLogin.module.css";
import { useNavigate } from "react-router-dom";

const ContenedorLogin = () => {
  const [started, setStarted] = useState(false);
  const [startEyes, setEyes] = useState(true);
const navigate = useNavigate();

  const initialValues = {
    email: "",
    password: "",
  };

  const validationSchema = Yup.object({
    email: Yup.string()
      .email("Dirección de correo inválida")
      .required("El Correo es un Campo requerido"),
    password: Yup.string()
      .required("La contraseña es un Campo requerido")
      // .matches(
      //   /^(?=.*[A-Z])(?=.*\d).*$/,
      //   "La contraseña debe contener al menos una mayúscula y un número"
      // ),
  });

  const handleSubmit = (values) => {
    // Agrega la lógica de manejo de form submit si es necesario
  };

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={handleSubmit}
    >
      {({ errors, touched }) => (
        <Form className={style.ContenedorFormulario}>
          <p className={style.p_isesion}>Iniciar sesión</p>

          <div className={style.label}>
            <Field
              id="loginUser"
              className={style.inputUsuario}
              type="text"
              name="email"
              onFocus={() => setStarted(true)}
              onBlur={() => setStarted(false)}
              placeholder=""
              autoComplete="off"
            />
            <span className={style.spanName}>Correo</span>
            <img
              className={style.iconUser}
              src={user0Icon}
              alt="Icono de usuario"
            />
          </div>

          <div className={style.label}>
            <Field
              id="loginPassword"
              className={style.inputContraseña}
              type={startEyes ? "password" : "text"}
              name="password"
              placeholder="Contraseña"
            />
            <img
              className={style.iconHide}
              src={startEyes ? hideIcon : insertIcon}
              alt="Icono de ocultar"
              onClick={() => {
                setEyes(!startEyes);
              }}
            />
          </div>

          {(errors.password || errors.email) &&
            (touched.password || touched.email) && (
              <div className={style.error_container}>
                {errors.email && touched.email && (
                  <ErrorMessage
                    name="email"
                    component="div"
                    className={style.error}
                  />
                )}

                {errors.password && touched.password && (
                  <ErrorMessage
                    name="password"
                    component="div"
                    className={style.error}
                  />
                )}
              </div>
            )}

          <button className={style.button} type="submit" onClick={() => navigate("/admin/")}>
            Entrar{" "}
            <img
              className={style.iconLogin}
              src={loginIcon}
              alt="Icono de login"
            />
          </button>
        </Form>
      )}
    </Formik>
  );
};

export default ContenedorLogin;
