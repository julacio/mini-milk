export const appRoutesBackOffice = [
  {
    path: "/admin/",
    sidebarProps: {
      displayText: "Dashboard",
    },
    // allowedRoles: ["admin", "operator"],
  },
  {
    path: "/admin/animales",
    sidebarProps: {
      displayText: "Gestión de Animales",
    },
    // allowedRoles: ["admin", "operator"],
  },
  {
    path: "/admin/precios",
    sidebarProps: {
      displayText: "Gestión de Precio",
    },
    // allowedRoles: ["admin", "operator"],
  },
  {
    path: "/admin/gastos-ingresos",
    sidebarProps: {
      displayText: "Gestión de Gastos / Ingresos",
    },
    // allowedRoles: ["admin", "operator"],
  },
];

export const specialRoute = [];

export const RouteUser = [];
