import React, { useState } from "react";
import { Navigate } from "react-router-dom";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import style from "./AppRoutes.module.css";

import Sidebar from "@/components/Sidebar/Sidebar";
import { MainHeader } from "@/components/MainHeader/MainHeader";
import { AppStructure } from "@/components/AppStructure/AppStructure";
import { appRoutesBackOffice, specialRoute, RouteUser } from "@/data/Rutas";

import { HomePage } from "@/features/web/pages/home/HomePage";
import { LoginPage } from "@/features/login/pages/LoginPage";

import { Dashboard } from "@/features/dashboard/pages/Dashboard";

import { useSelector, useDispatch } from "react-redux";

import { toggleSidebar } from "@/store/slices/sidebar/IsSidebarOpen";
import Animales from "@/features/Animales/Animales";
import Precios from "@/features/Precios/Precios";
import Gastos from "@/features/Gastos/Gastos";


export const AppRoutes = () => {
  const isSidebarOpen = useSelector((state) => state.sidebar.isSidebarOpen);
  const dispatch = useDispatch();

  const handleToggleSidebar = () => {
    dispatch(toggleSidebar());
  };

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<LoginPage />} />
 

        <Route
          path="/admin/*"
          element={
            <AppStructure>
              <MainHeader toggleSidebar={handleToggleSidebar} isSidebarOpen={isSidebarOpen} />
              <div
                className={style.container__drawer}
                style={{
                  display: isSidebarOpen ? "grid" : "flex",
                  gridTemplateColumns: isSidebarOpen ? "320px 1fr" : "none",
                  // justifyContent: isSidebarOpen ? "unset" : "center",
                }}
              >
                <Sidebar appRoutes={appRoutesBackOffice} isSidebarOpen={isSidebarOpen} specialRoute={specialRoute} RouteUser={RouteUser} />

                <Routes>
                  <Route path="/" element={<Dashboard />} />

                  {/* GESTION DE ANIMALES */}

                  <Route path="/animales" element={<Animales />} />

                  {/* GESTION DE PRECIOS */}

                  <Route path="/precios" element={<Precios />} />

                  {/* GESTION DE GASTOS / INGRESOS  */}

                  <Route path="/gastos-ingresos" element={<Gastos />} />
                  

                  {/* Agrega más rutas de sub-mantenimientos aquí */}
                  <Route path="/*" element={<Navigate to="/" />} />
                </Routes>
              </div>
            </AppStructure>
          }
        />
      </Routes>
    </BrowserRouter>
  );
};
